@extends('layouts.app')

@section('title', $page->title)

@section('content')
    @if ($page->view)
        {!! $page->view->render() !!}
    @else
        <section>
            <div class="container">
                {!! $page->content_html !!}
            </div>
        </section>
    @endif
@stop