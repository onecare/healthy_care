<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="keywords" content="," />
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://rawatsehat.com">
    <meta property="og:image" content="">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="600">
    <meta property="og:image:height" content="600">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@">
    <meta name="twitter:creator" content="@">
    <meta name="twitter:title" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:url" content="">
    <meta name="twitter:image" content="">
    <meta name="twitter:image:src" content="">

    <title>.:@yield('title'):.</title>

    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <!-- Custom CSS -->
    <link href="/css/temporary/ytv.css" type="text/css" rel="stylesheet" />
    <link href="/css/temporary/rawatsehat.css" type="text/css" rel="stylesheet" />
    {{-- <link href="/node_modules/flexslider/flexslider.css" type="text/css" rel="stylesheet" /> --}}
    {{-- <link href="css/temporary/video.css" type="text/css" rel="stylesheet" /> --}}
</head>
<body>

    {{-- <div class="container">
        <div class="header-connect" id="topnav">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"><i class="fa fa-navicon"></i>
            </button>
            <div class="text-center">
            <a href="index.html"><img src="/img/logo.png" alt="" class="logo"></a>
            </div>
            <button id="searchtoggle" class="navbar-toggle visible-xs" type="button"><i class="fa fa-search fa-lg"></i></button>
        </div>
    </div> --}}
    
    <!-- NAVIGATION -->
    {{-- <nav class="navbar template" role="navigation">
        <!-- MENU HEADER -->
        <div class="container text-center">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav inline">
                    @include('layouts.partials._navigation')           
                </ul>     
            </div>
        </div>
        <!-- END MENU HEADER -->
    </nav> --}}
    <!-- END NAVIGATION -->

    <!-- Navigation -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="logo">
                <a href="/"><img src="/img/logo.png" alt="" class="logo"></a>
            </div>
        </div>
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- <navbar->  </navbar->brand is hidden on larger screens, but visible when the menu is collapsed -->
                {{-- <a class="navbar-brand" href="/">Rawat Sehat</a> --}}
                <a class="navbar-brand" href="/"><img src="/img/logo.png" alt=""></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    @include('layouts.partials._navigation')           
                </ul> 
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    @yield('content')

    @include('layouts.partials._footer')

    <!-- BEGIN FOOTER TOP -->
    {{-- <div class="greyfooter">
        <div style="padding-top: 20px;">
            <div class="col-md-offset-1 col-md-3 footer1" >
                <img src="/img/logo.png" alt="">
            </div>
            <div class="col-md-3 footer2">
                <div class="col-md-12 footer-link footer-link-pad ">
                    <ul>
                    <li><a href="#">HOME</a></li>
                    <li><a href="#">FAQ</a></li>
                    <li><a href="{{ route('legal.terms') }}">SYARAT KETENTUAN</a></li>
                    <li><a href="{{ route('legal.privacy') }}">KEBIJAKAN PRIVASI</a></li>
                    <li><a href="#">KONTAK</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 footer3" >
                <h4><b>Keep Update</b></h4>
                <p>Enter Your Email Address to get information from Rawat Sehat</p>
                <div class="input-group" style="width: 85%;">
                    <span class="input-group-addon email"><i class="glyphicon glyphicon-envelope"></i></span>
                    <input type="text" class="form-control" placeholder="Masukkan email anda ..." aria-describedby="sizing-addon2">
                </div>
                <button type="submit" class="btn btn-light-blue" >SUBSCRIBE</button>
            </div>
        </div>
    </div> --}}
    <!-- END FOOTER TOP -->

    <!-- BEGIN FOOTER BOTTOM -->
    {{-- <div class="footer back-footer blackfooter">
        <div class="col-md-12" style="padding-top: 25px">
            <div class="col-md-offset-1 col-md-5">
                <ul class="icon">
                  <li><a href="https://www.facebook.com/rawatsehat" class="fb" target="_blank"><i class="fa fa-facebook-official"></i></a></li>
                  <li><a href="https://twitter.com/@rawat_sehat" class="twitter" target="_blank"><i class="fa fa-twitter"></i></a></li> 
                  <li><a href="https://www.instagram.com/rawatsehat" class="ig" target="_blank"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="https://www.youtube.com/channel/UCmQCwiVPfgJEye4E5TfjqLQ" class="twitter" target="_blank"><i class="fa fa-youtube"></i></a></li>
                </ul>
            </div>
            <div class="col-md-offset-3 col-md-3 bottom-link">
                <p>Copyright 2016 RawatSehat.</p>
            </div>
        </div>
    </div> --}}
    <!-- END FOOTER BOTTOM -->
</body>
<script src="/js/app.js"></script>
<!-- custom JS image & video-->
{{-- <script type="text/javascript" src="slick/slick.min.js"></script> --}}
<script type="text/javascript" src="/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="/js/masonry.pkgd.min.js"></script>
{{-- <script type="text/javascript" src="/js/back-to-top.js"></script> --}}
<script type="text/javascript" src="/js/temporary/ytv.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
      
      $('.mobile-search').click(function(e) {
        e.preventDefault();
        $('.search-mobile-form').slideToggle();
      });
      $('.navbar-toggle').click(function(e) {
        e.preventDefault();
        $('.navbar-mobile').slideToggle();
      });
      $('.nav-search').click(function(){
      })
      $('.nav-search').click(function(){
      })
      $('.nav-search').click(function(e){
        e.preventDefault()
      $('.btn-search').slideToggle();

      });

      $('li.nav-tabs-menu a').click(function(){
        $(this).parent().addClass('active');
        $(this).parent().siblings('li').removeClass('active');
      })

      $(".arrow-right").bind("click", function (event) {
            event.preventDefault();
            $(".vid-list-container").stop().animate({
                scrollLeft: "+=295"
            }, 750);
        });
        $(".arrow-left").bind("click", function (event) {
            event.preventDefault();
            $(".vid-list-container").stop().animate({
                scrollLeft: "-=295"
            }, 750);
        });
        $('.vid-item').click(function() {
          var vidTitle = $(this).find('.desc').html();
          var vidDesc = $(this).find('.thumb').attr('data-description');
          var vidSrc = $(this).attr('data-src');
          
          console.log(vidSrc);
          // alert(vidDesc);
          $('.textRight h2').html(vidTitle);
          $('.textRight h4').html(vidTitle);
          $('.textRight p').html(vidDesc);
          $('.vid_frame').attr('src',vidSrc);
        });

        $('#toggle-view li').click(function () {

            var text = $(this).children('div.collapse');

            if (text.is(':hidden')) {
                text.slideDown('200');
                $(this).children('span').html('-');		
            } else {
                text.slideUp('200');
                $(this).children('span').html('+');		
            }
            
        });
    });

    window.onload = function(){             
        window.controller = new YTV('frame', {
            user: 'collegehumor',
            accent: 'yellow'
        });
    };
</script>
@yield('script')
</html>