@foreach($pages as $page)
    <li class="{{ Request::is($page->uri_wildcard) ? 'active' : '' }} {{ count($page->children) ? ($page->isChild() ? 'dropdown-submenu' : 'dropdown') : '' }} nav-item">
        @if(count($page->children) && ! $page->isChild())
            <a href="{{ url($page->uri) }}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        @else
            <a href="{{ url($page->uri) }}">
        @endif
            {{ $page->title }}

            @if(count($page->children))
                <span class="caret {{ $page->isChild() ? 'right' : '' }}"></span>
            @endif
        </a>

        @if(count($page->children))
            <ul class="dropdown-menu">
                @include('layouts.partials._navigation', ['pages' => $page->children])
            </ul>
        @endif
    </li>
@endforeach