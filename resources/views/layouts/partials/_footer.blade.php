<!-- ******FOOTER****** --> 
<footer class="footer">
    <div class="footer-content">
        <div class="container">
            <div class="row">                    
                <div class="footer-col links col-md-4 col-sm-8 col-xs-12">
                    <div class="footer-col-inner">
                        <img src="/img/logo.png" alt="">
                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col-->               
                <div class="footer-col links col-md-2 col-sm-4 col-xs-12 sm-break">
                    <div class="footer-col-inner">
                        <h3 class="title">Support</h3>
                        <ul class="list-unstyled">
                            <li><a href="/faq"><i class="fa fa-caret-right"></i>FAQ</a></li>
                            <li><a href="{{ route('legal.terms') }}"><i class="fa fa-caret-right"></i>Syarat Ketentuan</a></li>
                            <li><a href="{{ route('legal.privacy') }}"><i class="fa fa-caret-right"></i>Kebijakan Privasi</a></li>
                            {{-- <li><a href="#"><i class="fa fa-caret-right"></i>Help</a></li>
                            <li><a href="#"><i class="fa fa-caret-right"></i>Documentation</a></li>
                            <li><a href="#"><i class="fa fa-caret-right"></i>Terms of services</a></li>
                            <li><a href="#"><i class="fa fa-caret-right"></i>Privacy</a></li> --}}
                        </ul>
                    </div><!--//footer-col-inner-->            
                </div><!--//foooter-col-->   
                <div class="footer-col connect col-xs-12 col-md-6">
                    <div class="footer-col-inner">
                        <ul class="social list-inline">
                            <li><a href="https://twitter.com/@rawat_sehat" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.facebook.com/rawatsehat"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/rawatsehat"><i class="fa fa-instagram"></i></a></li>        
                            <li><a href="https://www.youtube.com/channel/UCmQCwiVPfgJEye4E5TfjqLQ"><i class="fa fa-youtube"></i></a></li>      
                        </ul>
                        <div class="form-container">
                            <p class="intro">
                                Stay up to date with the latest news and offers from <b>Rawat Sehat</b><br>
                                <i><b>( Mohon maaf fitur ini masih non-aktif )</b></i>
                            </p>
                            <form class="signup-form navbar-form">
                                <div class="form-group">
                                    <input type="text" class="form-control" disabled="disabled" placeholder="Enter your email address">
                                </div>   
                                <button type="submit" class="btn btn-cta btn-cta-primary">Subscribe Now</button>                                 
                            </form>                               
                        </div><!--//subscription-form-->
                    </div><!--//footer-col-inner-->
                </div><!--//foooter-col-->
                <div class="clearfix"></div> 
            </div><!--//row-->
            <div class="row has-divider">
                <div class="footer-col download col-xs-12 col-md-6">
                    <div class="footer-col-inner">
                        <h3 class="title">Mobile apps</h3>
                        <ul class="list-unstyled download-list">
                            <li><a class="btn btn-ghost" href="https://goo.gl/A1N0AL"><i class="fa fa-apple"></i><span class="text">Download for iOS</span> </a></li>
                            <li><a class="btn btn-ghost" href="https://goo.gl/A1N0AL"><i class="fa fa-android"></i><span class="text">Download for Android</span></a></li>
                        </ul>
                    </div><!--//footer-col-inner-->
                </div><!--//download-->
                <div class="footer-col contact col-xs-12 col-md-6">
                    <div class="footer-col-inner">
                        <h3 class="title">Contact us</h3>                          
                        <p class="adr clearfix">
                            <i class="fa fa-map-marker pull-left"></i>        
                            <span class="adr-group pull-left">       
                                <span class="street-address">Jl. Mojo Kidul 52A</span><br>
                                <span class="region">Surabaya 60232, Indonesia</span><br>
                            </span>
                        </p>
                        <p class="tel"><i class="fa fa-phone"></i>082141070700</p>
                        <p class="email"><i class="fa fa-envelope-o"></i><a href="#">info@rawatsehat.com</a></p>                        
                    </div><!--//footer-col-inner-->
                </div><!--//contact-->
            </div>
        </div><!--//container-->
    </div><!--//footer-content-->
    <div class="bottom-bar">
        <div class="container">
            <small class="copyright">Copyright &copy; <a href="http://www.rawatsehat.com/" target="_blank">rawatsehat</a></small> 2016               
        </div><!--//container-->
    </div><!--//bottom-bar-->
</footer><!--//footer-->