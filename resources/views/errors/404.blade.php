<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
        .img {
            /* Set rules to fill background */
            min-height: 100%;
            min-width: 1024px;
                
            /* Set up proportionate scaling */
            width: 100%;
            height: auto;
                
            /* Set up positioning */
            position: fixed;
            top: 0;
            left: 0;
        }

        @media screen and (max-width: 1024px) { /* Specific to this particular image */
            .img {
                left: 50%;
                margin-left: -512px;   /* 50% */
            }
        }
    </style>
</head>
<body>
    <div class="img">
        <img src="/img/404.jpg" alt="not found"/>
    </div>
</body>
</html>