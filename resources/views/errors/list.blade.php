@if (count($errors) > 0)
    <div class="alert alert-danger alert-important">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
        @if($errors->all())
            <p> <i class="fa fa-warning"></i> <b>"Pengisian data anda belum lengkap"</b> atau <b>"Terjadi kesalahan pengisian data"</b>, mohon periksa kembali pengisian data anda!</p>
        @endif
    </div>
@endif