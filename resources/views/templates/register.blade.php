<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-heading">
                    Formulir Pendaftaran Online
                </h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                @include('flash::message')
                @include('errors.list')
                @include('templates.partials._formRegister')
                <blockquote>
                    <dl>
                        <dt>Syarat dan Ketentuan:</dt>
                        <dd>
                            <ul>
                                <li>Memiliki smartphone dengan sistem <b>android</b> atau <b>IOS</b></li>
                                <li>No. <b>STR</b> ( Surat Tanda Registrasi )</li>
                                <li>No. <b>SIP</b> ( Surat Ijin Praktek )</li>
                            </ul>
                        </dd>
                    </dl>
                </blockquote>
            </div>
        </div>
    </div>
</section>