<section id="articles">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-heading">Artikel</h2>
                <hr class="primary">
            </div>
        </div>

        <div class="row">
            <div id="article-mansonry" class="article-list">  
                @foreach ($articles as $article)
                   <article class="post col-md-4 col-sm-6 col-xs-12" >
                        <div class="post-inner">
                            <figure class="post-thumb">
                                <a href="{{ route('article.post', [$article->slug]) }}">
                                    @if($article->thumbnail_path)
                                        <img class="img-responsive" src="http://api.server.rawatsehat.net/{{ $article->thumbnail_path }}" alt="">
                                    @else
                                        <img class="img-responsive hidden-xs" src="http://www.placehold.it/360x240/253340/ffffff?text=no+image">
                                    @endif
                                    {{-- <img class="img-responsive" src="assets/images/blog/post-1-thumb.jpg" alt="" /> --}}
                                </a>                                
                            </figure><!--//post-thumb-->
                            <div class="content">
                                <h3 class="post-title"><a href="{{ route('article.post', [$article->slug]) }}" title="{{ $article->title }}">{{ str_limit($article->title, 40) }}</a></h3>
                                <div class="post-entry">
                                    <p>{!! str_limit($article->body_html, 200) !!}</p>
                                    <a class="read-more" href="{{ route('article.post', [$article->slug]) }}">Read more <i class="fa fa-long-arrow-right"></i></a>
                                </div>
                                <div class="meta">
                                    <ul class="meta-list list-inline">                                       
                                    	<li class="post-time post_date date updated">{{ $article->published_at }}</li>
                                    	<li class="post-author"> by <a href="{{ route('article.post', [$article->slug]) }}">{!! $article->author !!}</a></li>
                                    	<li class="post-comments-link">
                                	        <a href="#"><i class="fa fa-comments"></i>8</a>
                                	    </li>
                                	</ul><!--//meta-list-->                           	
                                </div><!--meta-->
                            </div><!--//content-->
                        </div><!--//post-inner-->
                    </article><!--//post-->
                @endforeach
            </div>
        </div>

        <!-- PAGINATION -->
        <div class="row">
            <div class="pagination-container text-center">
                {{ $articles->links() }}
            </div>
        </div>
        <!-- PAGINATION -->

        </div>  
    </div>
</section>