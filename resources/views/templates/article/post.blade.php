<section id="article-post">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="section-heading-article">
                    {!! $article->title !!}
                </h2>
                <hr class="head-line-article pull-left">
                <div class="clearfix"></div>
                <span class="published-article pull-left">
                    Posted <b>{{ $article->PublishedAtLocalized }}</b> &bullet; by <b>{{ $article->author }}</b>
                </span>              
            </div>
        </div>
        <div class="row article-post">
            <div class="col-md-8">
                @unless (empty($article->path))
                    <img src="http://api.server.rawatsehat.net/{{ $article->path }}" class="img-responsive text-center" alt="image">
                @endif
                </br>
                {!! $article->body_html !!}
            </div>
            <div class="col-md-4">
                <div class="row article-right">
                    <h3>Artikel Lain</h3>
                </div>
                <div class="row">
                    @foreach ($articles as $other)
                        <div class="col-md-12">
                            <h5><b><a href="{{ route('article.post', [$other->slug]) }}">{{ $other->title }}</a></b></h5>
                            <div class="artikel-time"> {{ $other->PublishedAtLocalized }}
                                <span class="fa fa-comments pull-right" aria-hidden=""> 4</span>
                                <hr class="line-14">
                            </div>
                        </div>
                    @endforeach
                    {{ $articles->links() }}
                </div>       
            </div>
        </div>
    </div>
</section>