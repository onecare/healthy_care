<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-heading">
                    Kontak
                </h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <p>
                    <b>Jl. Mojo Kidul 52A</b></br>
                    Surabaya 60232, Indonesia</br></br>
                    Email: info@rawatsehat.com</br>
                    Phone: 082141070700</br>
                    {{-- Website: www.rawatsehat.com</br>
                    Facebook: Rawat Sehat</br>
                    Twitter: @rawat_sehat</br>
                    Instagram: rawatsehat --}}
                </p>
                <div style="height:350px;width:100%;max-width:100%;list-style:none; transition: none;overflow:hidden;"><div id="my-map-canvas" style="height:100%; width:100%;max-width:100%;"><iframe style="height:100%;width:100%;border:0;" frameborder="0" src="https://www.google.com/maps/embed/v1/search?q=Jalan+Mojo+Kidul+52A&key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU"></iframe></div><a class="google-map-code" href="http://www.szablonypremium.pl" id="enable-map-data">http://www.szablonypremium.pl</a><style>#my-map-canvas .text-marker{max-width:none!important;background:none!important;}img{max-width:none}</style></div>
            </div>
            <div class="col-md-6">
                <form action="">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" class="form-control" id="name" placeholder="Name">
                    </div>
                    
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
                    </div>

                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" class="form-control" id="message" rows="5" placeholder="Message"></textarea>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-10 col-sm-2">
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>