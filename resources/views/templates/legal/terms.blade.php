<section id="legal_info">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-heading">
                    @foreach ($translations as $translation)     
                        {!! $translation->title !!}
                    @endforeach
                </h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @foreach ($translations as $translation)     
                    {!! $translation->body_html !!}
                @endforeach
            </div>
        </div>
    </div>
</section>