<section class="slider">
    <div class="flexslider img-slider">
        <ul class="slides">
            <li class="slide">
                <img src="img/banner-registrasi-02.jpg" />
            </li>
            <li class="slide">
  	    	    <img src="img/slide_web-01.jpg"/>
            </li>
            <li class="slide">
  	    	    <img src="img/slider1.jpg"/>
            </li>
        </ul>
    </div>
</section>

<div class="container bg-white">
    <img src="img/alur_registrasi_rawat_sehat.png" class="img-responsive"/>
    <hr>
</div>

<!-- BEGIN CONTENT 1-->
<div class="container">
    <!-- Begin Content -->      
    <div class="col-md-4 page-1">
        <a href="#"><img src="img/nurse1.png" alt="" class="img-nurse"></a>
    </div>

    <div class="col-md-8 p-right">
        <h4>APA ITU <br> RAWAT SEHAT ?</h4>

        <hr class="head-line pull-left"><br><br>

        <p>{!! str_limit($about->content_html, 250) !!}</p>
        </br>
        <a href="/about" class="btn-more btn"> Read More</a>
    </div>
</div>
<!-- END CONTENT 1 -->

<!-- BEGIN CONTENT 2 -->
<div class="main-banner"></div>
    <div class="container page-2">
        <div class="col-md-offset-2 col-md-5">
        <h4> RAWAT SEHAT APPS</h4>
        {!! $pages->content_html !!}

        <div class="col-md-12 store">
            <center><h5>DOWNLOAD DI:</h5></center>
            <a href="https://goo.gl/A1N0AL" target="_blank"><img src="img/appstore.png" class="text-left iconStore" alt=""></a>
            <a href="https://goo.gl/A1N0AL" target="_blank"><img src="img/googleplay.png" class="text-right iconStore" alt=""></a>
        </div>
    </div>

    <div class="col-md-5 apps">
        <a href="#"><img src="img/apps.png" alt="" class="img-apps"></a>
    </div>
</div>
<!-- END CONTENT 2 -->

<!-- BEGIN CONTENT 3 -->
<div class="list-video">
    <div class="container video-post text-center">
        <center><h3><b>VIDEO</b></h3></center>
        <hr class="head-line-1">
        <iframe  id="vid_frame" class="vid_frame" src="http://youtube.com/embed/WJL-v4Yg0lw?autoplay=0&rel=0&showinfo=0" frameborder="0" width="70%" height="400"></iframe>
        <br><br>
    </div> 
</div>
<!-- END CONTENT 3 -->

<!-- BEGIN CONTENT 4 -->
{{-- <div  id="quotes-section" class="row">                                
    <div id="myCarousel2" class="carousel slide col-md-12" data-ride="carousel"> 
        <h2>Kata Mereka</h2>
        <ol class="carousel-indicators show">
            <li data-target="#myCarouse2" data-slide-to="0" class="active"></li>
            <li data-target="#myCarouse2" data-slide-to="1"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <div class="item active row">
                <div class="col-md-offset-2 col-md-2">
                <img src="img/photo.png">
                </div>
                <div class="col-md-8 quotes">
                <blockquote>
                    <i class="fa fa-quote-left"></i>
                    <p>" Layanan ini sangat membantu untuk menambah panghasilan bagi saya "</p>
                    <p><b> Dr Casey Kasianto, Dokter Spesialis paru</b></p>
                </blockquote> 
                </div>
            </div>

            <div class="item row">
                <div class="col-md-offset-2 col-md-2">
                <img src="img/photo.png">
                </div>
                <div class="col-md-8 quotes">
                <p>Rawat sehat sangat membantu saya dalam merawat ibu saya</p>
                <p><b> Muhammad Ali, pasien</b></p>
                </div>
            </div>
        </div>
    </div>
</div> --}}

<section class="section testimonials">
    <div class="container">
        <h2 class="title text-center">Kata Mereka</h2>
        <hr class="primary">
        <div id="testimonials-carousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators show">
                @foreach ($testimonials as $i => $testimonial)
                    <li data-target="#testimonials-carousel" data-slide-to="{{$i}}" class="{{ $i == 0 ? 'active' : '' }}"></li>
                @endforeach
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox"> 
                @foreach ($testimonials as $i => $testimonial)
                    <div class="item {{ $i == 0 ? 'active' : '' }}">
                        <figure class="profile"><img src="img/photo.png" alt="" /></figure>
                        <div class="content">
                            <blockquote>
                            <i class="fa fa-quote-left"></i>
                            {!! $testimonial->review !!}
                            </blockquote>
                            <p class="source">{!! $testimonial->author !!}<br /><span class="title">{!! $testimonial->designation !!}</span></p>
                        </div><!--//content-->                         
                    </div><!--//item-->
                @endforeach
            </div>
        </div>
    </div>
</section>
<!-- END CONTENT 4 -->