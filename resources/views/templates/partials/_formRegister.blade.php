{!! Form::open(['method' => 'POST', 'action' => 'RegisterController@store']) !!}
    <div class="row" id="services">
        <div class="col-md-12">
            <div class="service-item">
                <h3>{{ $service_img->name }}</h3>
                <img src="http://api.server.rawatsehat.net/{{ $service_img->path_logo }}" class="img-responsive" alt=""> 
                
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::label('service_id', 'Saya mendaftar di pelayanan...', ['class' => 'control-label']) !!}
                {!! Form::select('service_id', $services, null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="form-group @if ($errors->has('first_name')) has-error @endif">
                {!! Form::label('first_name', $errors->has("first_name") ? 'Nama Depan' . $errors->first("first_name") : 'Nama Depan', ['class' => 'control-label bold']) !!}            
                {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'Nama Depan']) !!}
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group @if ($errors->has('last_name')) has-error @endif">
                {!! Form::label('last_name', $errors->has("last_name") ? 'Nama Belakang' . $errors->first("last_name") : 'Nama Belakang', ['class' => 'control-label bold']) !!}            
                {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Nama Belakang']) !!}
                <span id="helpBlock" class="help-block">
                    <i class="fa fa-question-circle"></i>  
                    Jika nama anda hanya terdiri dari 1 kata, mohon isi input <b>nama belakang</b> dengan <b>gelar akademik/title</b> yang anda miliki. </br>
                    <i class="fa fa-question-circle"></i>
                    Jika nama anda lebih dari 1 kata, mohon isi input <b>nama belakang</b> dengan kata terakhir dari nama anda dan disertai dengan <b>gelar akademik/title yang anda miliki.</b>
                </span>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">                   
            <div class="form-group @if ($errors->has('email')) has-error @endif">
                {!! Form::label('email', $errors->has("email") ? 'Email' . $errors->first("email") : 'Email', ['class' => 'control-label bold']) !!}           
                {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group @if ($errors->has('gender')) has-error @endif">
                {!! Form::label('gender', $errors->has("gender") ? 'Gender' . $errors->first("gender") : 'Gender', ['class' => 'control-label bold']) !!}            
                    {!! Form::select('gender', [
                        '1' => 'Laki-laki',
                        '2' => 'Perempuan',
                    ], null, ['class' => 'form-control', 'placeholder' => 'Saya...']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12"> 
            <div class="form-group @if ($errors->has('identity_number')) has-error @endif">
                {!! Form::label('identity_number', $errors->has("identity_number") ? 'No. KTP/NIK' . $errors->first("identity_number") : 'No. KTP/NIK', ['class' => 'control-label bold']) !!}            
                {!! Form::number('identity_number', null, ['class' => 'form-control', 'placeholder' => 'No. KTP', 'min' => '0']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 @if ($errors->has('day') || $errors->has('month') || $errors->has('year')) has-error @endif">
            {!! Form::label('date_of_birth', 'Tanggal Lahir', ['class' => 'control-label bold']) !!}
        </div>
        <div class="col-md-2">
            <div class="form-group @if ($errors->has('day')) has-error @endif">
                {!! Form::number('day', null, ['class' => 'form-control', 'placeholder' => 'Tgl', 'min' => '1', 'max' => 31]) !!}
                @if ($errors->has('day'))
                    <span class="help-block">{!! $errors->first('day') !!}</span>
                @endif
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group @if ($errors->has('month')) has-error @endif">
                {!! Form::selectMonth('month', null, ['class' => 'form-control', 'placeholder' => 'Bulan']) !!}
                @if ($errors->has('month'))
                    <span class="help-block">{!! $errors->first('month') !!}</span>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group @if ($errors->has('year')) has-error @endif">
                {!! Form::text('year', null, ['class' => 'form-control', 'placeholder' => 'Tahun']) !!}
                @if ($errors->has('year'))
                    <span class="help-block">{!! $errors->first('year') !!}</span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">        
            <div class="form-group @if ($errors->has('birthplace')) has-error @endif">
                {!! Form::label('birthplace', $errors->has("birthplace") ? 'Tempat Lahir' . $errors->first("birthplace") : 'Tempat Lahir', ['class' => 'control-label bold']) !!}
                {!! Form::text('birthplace', null, ['class' => 'form-control', 'placeholder' => 'Tempat Lahir']) !!}
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">       
            <div class="form-group @if ($errors->has('religion')) has-error @endif">
                {!! Form::label('religion', $errors->has("religion") ? 'Agama' . $errors->first("religion") : 'Agama', ['class' => 'control-label bold']) !!}            
                {!! Form::select('religion', [
                    'islam' => 'Islam',
                    'kristen' => 'Kristen',
                    'katolik' => 'Katolik',
                    'hindu' => 'Hindu',
                    'buddha' => 'Buddha',
                    'konghucu' => 'Kong Hu Cu',
                ], null, ['class' => 'form-control', 'placeholder' => 'Agama saya...']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">         
            <div class="form-group @if ($errors->has('marital_status')) has-error @endif">
                {!! Form::label('marital_status', $errors->has("marital_status") ? 'Status Pernikahan' . $errors->first("marital_status") : 'Status Pernikahan', ['class' => 'control-label bold']) !!}            
                {!! Form::select('marital_status', [
                    'married' => 'Married',
                    'single' => 'Single',
                    'widowed' => 'Widowed',
                    'divorced' => 'Divorced',
                ], null, ['class' => 'form-control', 'placeholder' => 'Status pernikahan saya...']) !!}
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">          
            <div class="form-group @if ($errors->has('address')) has-error @endif">
                {!! Form::label('address', $errors->has("address") ? 'Alamat' . $errors->first("address") : 'Alamat', ['class' => 'control-label bold']) !!}            
                {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Alamat']) !!}
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">              
            <div class="form-group @if ($errors->has('city')) has-error @endif">
                {!! Form::label('city', $errors->has("city") ? 'Kota' . $errors->first("city") : 'Kota', ['class' => 'control-label bold']) !!}
                {!! Form::select('city', $cities, null, ['class' => 'form-control', 'placeholder' => '']) !!} 
                {{-- {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'Kota']) !!} --}}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">                    
            <div class="form-group @if ($errors->has('phone')) has-error @endif">
                {!! Form::label('phone', $errors->has("phone") ? 'Nomor Telepon' . $errors->first("phone") : 'Nomor Telepon', ['class' => 'control-label bold']) !!}            
                {!! Form::number('phone', null, ['class' => 'form-control', 'placeholder' => 'Nomor Telepon', 'min' => '0']) !!}
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">                          
            <div class="form-group @if ($errors->has('last_education')) has-error @endif">
                {!! Form::label('last_education', $errors->has("last_education") ? 'Pendidikan Terakhir' . $errors->first("last_education") : 'Pendidikan Terakhir', ['class' => 'control-label bold']) !!}            
                {!! Form::select('last_education', [
                    'D3' => 'Diploma 3',
                    'S1' => 'S1',
                    'S2' => 'S2',
                    'S3' => 'S3',
                    'Dokter' => 'Dokter',
                ], null, ['class' => 'form-control', 'placeholder' => 'Pendidikan terakhir saya...']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">                           
            <div class="form-group @if ($errors->has('education_place')) has-error @endif">
                {!! Form::label('education_place', $errors->has("education_place") ? 'Tempat Pendidikan' . $errors->first("education_place") : 'Tempat Pendidikan', ['class' => 'control-label bold']) !!}
                {!! Form::text('education_place', null, ['class' => 'form-control', 'placeholder' => 'Tempat pendidikan saya...']) !!}
                {{-- {!! Form::select('education_place_id', $education_place, null, ['class' => 'form-control', 'placeholder' => 'Tempat pendidikan saya...']) !!} --}}
            </div>
        </div>
        <div class="col-md-4">                           
            <div class="form-group @if ($errors->has('graduation_year')) has-error @endif">
                {!! Form::label('graduation_year', $errors->has("graduation_year") ? 'Tahun Lulus' . $errors->first("graduation_year") : 'Tahun Lulus', ['class' => 'control-label bold']) !!}
                {!! Form::selectYear('graduation_year', Carbon\Carbon::now()->subYears(30)->format('Y'), Carbon\Carbon::now()->format('Y'), null, ['class' => 'form-control', 'placeholder' => 'Tahun lulus saya..']) !!}
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-8">                            
            <div class="form-group @if ($errors->has('str_number')) has-error @endif">
                {!! Form::label('str_number', $errors->has("str_number") ? 'No. STR' . $errors->first("str_number") : 'No. STR', ['class' => 'control-label bold']) !!}            
                {!! Form::text('str_number', null, ['class' => 'form-control', 'placeholder' => 'No. STR']) !!}
            </div>
        </div>
        <div class="col-md-4">                            
            <div class="form-group @if ($errors->has('expired_date_str')) has-error @endif">
                {!! Form::label('expired_date_str', $errors->has("expired_date_str") ? 'Masa Berlaku' . $errors->first("expired_date_str") : 'Masa Berlaku', ['class' => 'control-label bold']) !!}            
                {!! Form::text('expired_date_str', null, ['class' => 'form-control', 'placeholder' => 'Masa Berlaku STR']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">                            
            <div class="form-group @if ($errors->has('sip_number')) has-error @endif">
                {!! Form::label('sip_number', $errors->has("sip_number") ? 'No. SIP' . $errors->first("sip_number") : 'No. SIP', ['class' => 'control-label bold']) !!}            
                {!! Form::text('sip_number', null, ['class' => 'form-control', 'placeholder' => 'No. SIP']) !!}
            </div>
        </div>
        <div class="col-md-4">                            
            <div class="form-group @if ($errors->has('expired_date_sip')) has-error @endif">
                {!! Form::label('expired_date_sip', $errors->has("expired_date_sip") ? 'Masa Berlaku' . $errors->first("expired_date_sip") : 'Masa Berlaku', ['class' => 'control-label bold']) !!}            
                {!! Form::text('expired_date_sip', null, ['class' => 'form-control', 'placeholder' => 'Masa Berlaku SIP']) !!}
            </div>
        </div>
    </div>

    <hr>
    <div class="row">
        <div class="col-md-12">                            
            <div class="form-group @if ($errors->has('profession_card_number')) has-error @endif">
                {!! Form::label('profession_card_number', $errors->has("profession_card_number") ? 'No. Anggota Profesi' . $errors->first("profession_card_number") : 'No. Anggota Profesi', ['class' => 'control-label bold']) !!}            
                {!! Form::text('profession_card_number', null, ['class' => 'form-control', 'placeholder' => 'No. Anggota Profesi']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">                            
            <div class="form-group @if ($errors->has('workplace')) has-error @endif">
                {!! Form::label('workplace', $errors->has("workplace") ? 'No. Tempat Kerja' . $errors->first("workplace") : 'No. Tempat Kerja', ['class' => 'control-label bold']) !!}            
                {!! Form::text('workplace', null, ['class' => 'form-control', 'placeholder' => 'Tempat Kerja']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">                  
            <div class="form-group @if ($errors->has('work_experience')) has-error @endif">
                {!! Form::label('work_experience', $errors->has("work_experience") ? 'Pengalaman Kerja' . $errors->first("work_experience") : 'Pengalaman Kerja', ['class' => 'control-label bold']) !!}            
                {!! Form::textarea('work_experience', null, ['class' => 'form-control', 'placeholder' => 'Pengalaman Kerja']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                {!! Form::button('Daftar', ['type' => 'submit' ,'class' => 'btn btn-primary', 'id' => 'register-btn', 'autocomplete' => 'off', 'data-loading-text' => 'Mohon tunggu, proses registrasi sedang berlangsung...']) !!}	
            </div>
        </div>
    </div>
{!! Form::close() !!}