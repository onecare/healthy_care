<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-heading">Layanan Kami</h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @foreach ($services as $service)     
                    <div class="col-md-4">
                        <div class="service-item">
                            <img src="http://api.server.rawatsehat.net/{{ $service->path_logo }}" class="img-responsive" alt="">
                            <h3>{{ $service->name }}</h3>
                            {!! $service->description_html !!}
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>