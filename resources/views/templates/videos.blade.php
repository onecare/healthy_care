<section id="articles">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-heading">Video</h2>
                <hr class="primary">
            </div>
        </div>

        <div class="row">
            <div id="video-mansonry" class="article-list">  
                @foreach ($videos as $video)
                   <article class="post col-md-4 col-sm-6 col-xs-12">
                        <div class="post-inner">
                            <figure class="post-thumb">
                                <a href="{{ route('video.post', [$video->slug]) }}">
                                     <img class="img-responsive" src="https://img.youtube.com/vi/{{ substr($video->src, 32) }}/mqdefault.jpg">
                                    {{-- @if($video->thumbnail_path)
                                        <img class="img-responsive hidden-xs" src="http://api.server.rawatsehat.net/{{ $video->thumbnail_path }}" alt="">
                                    @else
                                        <img class="img-responsive hidden-xs" src="http://www.placehold.it/360x240/253340/ffffff?text=no+image">
                                    @endif --}}
                                    {{-- <img class="img-responsive" src="assets/images/blog/post-1-thumb.jpg" alt="" /> --}}
                                </a>                                
                            </figure><!--//post-thumb-->
                            <div class="content">
                                <h3 class="post-title"><a href="{{ route('video.post', [$video->slug]) }}" title="{{ $video->title }}">{{ str_limit($video->title, 50) }}</a></h3>
                                <div class="post-entry">
                                    <p>{!! str_limit($video->body_html, 120) !!}</p>
                                </div>
                                <div class="meta">
                                    <ul class="meta-list list-inline">                                       
                                    	<li class="post-time post_date date updated">{{ $video->published_at_diff }}</li>
                                	</ul><!--//meta-list-->
                                </div><!--meta-->
                            </div><!--//content-->
                        </div><!--//post-inner-->
                    </article><!--//post-->
                @endforeach
            <div class="clearfix"></div>
        </div>

        <!-- PAGINATION -->
        <div class="row">
            <div class="pagination-container text-center">
                {{ $videos->links() }}
            </div>
        </div>
        <!-- PAGINATION -->

        </div>  
    </div>
</section>