<section id="article-post">
    <div class="container">
        <div class="row article-post">
            <div class="col-md-7">
                <!-- THE YOUTUBE PLAYER -->
              <iframe  id="vid_frame" class="vid_frame" src="http://youtube.com/embed/{{ substr($video->src, 32) }}?autoplay=0&rel=0&showinfo=0" frameborder="0" width="100%" height="400"></iframe>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="section-heading-article">
                            {!! $video->title !!}
                        </h2>
                        <hr class="head-line-article pull-left">
                        <div class="clearfix"></div>
                        <span class="published-article pull-left">
                            Posted <b>{{ $video->PublishedAtLocalized }}</b>
                        </span> 
                    </div>       
                </div>

                 <div class="row article-post">
                    <div class="col-md-12">
                        {!! $video->description_html !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="">
                <div class="vid-list-container">
                    <div class="article-list vid-list">  
                        @foreach ($videos as $video)
                        <article class="post col-md-3 col-sm-6 col-xs-12 vid-item">
                                <div class="post-inner">
                                    <figure class="post-thumb">
                                        <a href="{{ route('video.post', [$video->slug]) }}">
                                            <img class="img-responsive hidden-xs" src="https://img.youtube.com/vi/{{ substr($video->src, 32) }}/mqdefault.jpg">
                                            {{-- @if($video->thumbnail_path)
                                                <img class="img-responsive hidden-xs" src="http://api.server.rawatsehat.net/{{ $video->thumbnail_path }}" alt="">
                                            @else
                                                <img class="img-responsive hidden-xs" src="http://www.placehold.it/360x240/253340/ffffff?text=no+image">
                                            @endif --}}
                                            {{-- <img class="img-responsive" src="assets/images/blog/post-1-thumb.jpg" alt="" /> --}}
                                        </a>                                
                                    </figure><!--//post-thumb-->
                                    <div class="content">
                                        <h3 class="post-title"><a href="{{ route('video.post', [$video->slug]) }}" title="{{ $video->title }}">{{ str_limit($video->title, 50) }}</a></h3>
                                        <div class="post-entry">
                                            <p>{!! str_limit($video->body_html, 120) !!}</p>
                                        </div>
                                        <div class="meta">
                                            <ul class="meta-list list-inline">                                       
                                                <li class="post-time post_date date updated">{{ $video->published_at_diff }}</li>
                                            </ul><!--//meta-list-->
                                        </div><!--meta-->
                                    </div><!--//content-->
                                </div><!--//post-inner-->
                            </article><!--//post-->
                        @endforeach
                    <div class="clearfix"></div>
                </div>
                </div>

                <div class="vid-arrows-container hidden-xs">
                    <div class="arrows">
                    <div class="arrow-left glyphicon glyphicon-menu-left"></div>
                    <div class="arrow-right glyphicon glyphicon-menu-right"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>