  <section id="about">
    <!-- BEGIN CONTENT 1-->
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-heading">Tentang Kami</h2>
                <hr class="primary">
            </div>
        </div>
        <!-- Begin Content -->      
        <div class="col-md-4 page-1">
        <a href="#"><img src="img/nurse1.png" alt="" class="img-nurse"></a>
        </div>

        <div class="col-md-8 p-right">
        <h4>APA ITU <br> RAWAT SEHAT?</h4>

        <hr class="head-line pull-left"><br><br>

        {!! $pages->content_html !!}
        </div>
    </div>
    <!-- END CONTENT 1 -->
  </section>

  {{-- <!-- BEGIN CONTENT 2 -->
  <div class="main-banner"></div>
    <div class="container page-2">
      <div class="col-md-offset-2 col-md-5">
      <h4> RAWAT SEHAT APPS</h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>

      <div class="col-md-10 store">
      <center><h5>DOWNLOAD DI:</h5></center>
      <a href="#"><img src="img/appstore.png" class="text-left iconStore" alt=""></a>
      <a href="#"><img src="img/googleplay.png" class="text-right iconStore" alt=""></a>
      </div>
      </div>

      <div class="col-md-5 apps">
        <a href="#"><img src="img/apps.png" alt="" class="img-apps"></a>
      </div>
    </div>
    <!-- END CONTENT 2 --> --}}