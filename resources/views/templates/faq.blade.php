<section id="legal_info">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h2 class="section-heading">
                    F.A.Q
                </h2>
                <hr class="primary">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul id="toggle-view">
                    @foreach ($translations as $translation)     
                        <li class="faq-content">
                            <p class="faq-title">{!! $translation->question !!}</p>
                            <span>+</span>
                            <div class="collapse">
                                {!! $translation->answer_html !!}
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>