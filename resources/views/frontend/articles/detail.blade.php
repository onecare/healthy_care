@extends('layouts.app')

@section('content')
	

     </div>
     	<div id="faq" class="row">
            <div class="col-lg-8 artikel-post col-md-offset-2">
                <!-- Title -->
                <h1 class="center">{{$article->title}}</h1>

                <!-- Date/Time -->
                <p class="center"><span class="glyphicon glyphicon-time"></span> Posted on {{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $article->created_at)->toDayDateTimeString()}}</p>

                <hr>

                <!-- Preview Image -->
                <img class="img-responsive" src="http://placehold.it/900x300" alt="">

                <hr>

                <!-- Post Content -->
                <p class="lead">{!! $article->excerpt !!}</p>
                {!! $article->body !!}

                <hr>

                <!-- Blog Comments -->

                <!-- Comments Form -->
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form">
                        <div class="form-group">
                            <textarea class="form-control" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

                <hr>

                
            </div>
      </div>
@stop