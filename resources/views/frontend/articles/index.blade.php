@extends('layouts.app')

@section('content')
	

     </div>
     	<div id="faq" class="row">
               <div id="title" class="col-md-3">
                    <h2 class="pull-right">ARTIKEL<br>KESEHATAN</h2>
               </div>
               <div class="col-md-9">
               		<div class="container-fluid text-center bg-grey">
						<div class="row artikel">
             @foreach ($articles as $article)
    						<div class="col-sm-4">
                 <div class="thumbnail">
                   <img src="{{$article->thumbnail_path}}" alt="{{$article->thumbnail_path}}">
                   {{  Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $article->created_at)->toFormattedDateString()  }}
                   <p><strong>{{$article->title}}</strong></p>
                   <p>{{$article->excerpt}}</p>
                    <ul class="actions">
                      <li><a href="{{ url('/article/'.$article->id) }}" class="button">More</a></li>
                    </ul>
                 </div>
                </div>
                @endforeach 
               
            </div>
            <div class="row">
              <div class="col-sm-4" >                
               {{ $articles->links() }}
              </div>
            </div>
               		</div>
                  
                    
               </div>
               

          </div>
@stop