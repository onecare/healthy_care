@extends('layouts.app')

@section('content')
	

     </div>
     	<div id="contact" class="row">
               <div id="title" class="col-md-3">
                    <h2 class="pull-right">HUBUNGI KAMI</h2>
               </div>
               <div class="col-md-9 no-pad">
                   <img class="contact-banner" src="/images/contact-banner.jpg">
                    
               </div>
               

          </div>

        <div class="inner">
        	<div class="row">
        		<div class="col-md-12">
        		<h2>Hubungi Kami</h2>
        		</div>
        	</div>

        	<div class="row">
        		<div class="col-md-8 contact-form">
					<form>
					  <div class="form-group">
  						  <label for="nama">Nama :</label>
  						  <input type="text" class="form-control" id="nama">
  					  </div>

					  <div class="form-group">
  						  <label for="email">Alamat Email :</label>
  						  <input type="email" class="form-control" id="email">
  					  </div>

  					   <div class="form-group">
  						  <label for="telepon">Nomor Telepon :</label>
  						  <input type="text" class="form-control" id="telepon">
  					  </div>

  					     <div class="form-group">
  						  <label for="subyek">Subyek :</label>
  						  <input type="text" class="form-control" id="subyek">
  					  </div>
						
						<div class="form-group">
						  <label for="pesan">Pesan :</label>
  							<textarea class="form-control" rows="7" id="pesan"></textarea>
						</div>

						<a href="#" class="btn-contact">KIRIM</a>	

  					 </form>
        		</div>

        		<div id="detail" class="col-md-4">
        			<h3>CONTACT DETAIL</h3>
        			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."        				
        			</p>

              <p><strong>Alamat</strong> : jalan Mojo Kidul 52 A, Surabaya </p>
              <p><strong>Phone</strong> : belum</p>
              <p><strong>Email</strong> : belum </p>

        		</div>

        	</div>


        </div>  
        <br></br>
@stop