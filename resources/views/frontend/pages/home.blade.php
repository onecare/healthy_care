@extends('layouts.app')

@section('content')
						
                            <!-- Banner -->
                            
                        </div>
                        
                        <div id="myCarousel" class="carousel slide " data-ride="carousel">
  
                            <ol class="carousel-indicators">
                             <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                             <li data-target="#myCarousel" data-slide-to="1"></li>
                             <li data-target="#myCarousel" data-slide-to="2"></li>
                             <li data-target="#myCarousel" data-slide-to="3"></li>
                              <li data-target="#myCarousel" data-slide-to="4"></li>
                              <li data-target="#myCarousel" data-slide-to="5"></li>
                              <li data-target="#myCarousel" data-slide-to="6"></li>
                            </ol>

                             <div class="carousel-inner" role="listbox">
                                 <div class="item active">
                                     <img src="/images/slider1.jpg">
                                 </div>

                                  <div class="item">
                                     <img src="/images/slider2.jpg">
                                 </div>


                                  <div class="item">
                                     <img src="/images/slider3.jpg">
                                 </div>


                                  <div class="item">
                                     <img src="/images/slider4.jpg">
                                 </div>


                                  <div class="item">
                                     <img src="/images/slider5.jpg">
                                 </div>


                                  <div class="item">
                                     <img src="/images/slider6.jpg">
                                 </div>
                             </div>

                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                             </a>
                            
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                            </div>

                        <div class="inner">
                                <section id="banner">
                                    <div class="content">
                                        <header>
                                            <h1>APA ITU<br />
                                            RAWAT SEHAT?</h1>                                     
                                        </header>
                                        <div id="message-about">
                                        <p>Kesehatan dan Teknologi telah berjalan beriringan, dan masing-masing telah berkembang dengan sangat cepat, 
                                        sehingga mempengaruhi pola prilaku manusia dalam memperoleh kesehatannya.</p>
                                             <p>Kesehatan dan Teknologi telah berjalan beriringan, dan masing-masing telah berkembang dengan sangat cepat, 
                                        sehingga mempengaruhi pola prilaku manusia dalam memperoleh kesehatannya.</p>
                                        <div>
                                        <p>Rawat sehat sebagai bagian dari pengembangan profesi kesehatan telah membaca peluang dengan memanfaatkan kemajuan 
                                            tekhnologi tersebut khususnya tekhnologi berbasis aplikasi.</p>
                                        <p>Rawat sehat tahu akan kebutuhan pelayanan kesehatan anda dalam genggaman smartphone anda dengan layanan yang mudah 
                                            terakses setiap saat.</p>
                                        <p>kami hadir memang untuk anda</p>   
                                        
                                        </div>                                        
        
                                        <ul class="actions readmore">
                                            <li><a href="#" class="button big">Read More</a></li>
                                        </ul>
                                        </div>
                                        
                                    </div>
                                    <span class="image object">
                                        <img src="images/nurse.png" alt="" />
                                    </span>
                                </section>

                            <!-- Section -->
                                <section>
                                    <header class="major">
                                        <h2>Layanan Kami</h2>
                                    </header>
                                    <div class="features">
                                        <article>
                                            <!--<span class="icon fa-diamond"></span> -->
                                            <img  class="icon-layanan" src="/images/dokter.png" />
                                            <div class="content">
                                                <h3>Dokter</h3>
                                                <p>Pelayanan home visit maupun konsultasi secara langsung dengan dokter terpercaya anda.</p>
                                            </div>
                                        </article>
                                        <article>
                                            <img class="icon-layanan" src="/images/perawat.png" />
                                            <div class="content">
                                                <h3>Perawat</h3>
                                                <p>Berbagai layanan tindakan keperawatan mulai pemberian obat sampai perawatan luka akan lebih dekat dengan anda.</p>
                                            </div>
                                        </article>
                                        <article>
                                            <img class="icon-layanan" src="/images/bidan.png" />
                                            <div class="content">
                                                <h3>Bidan</h3>
                                                <p>Berbagai layanan tindakan kebidanan mulai dari perawatan ibu sampai perawatan bayi akan lebih dekat dengan anda .</p>
                                            </div>
                                        </article>
                                        <article>
                                            <img class="icon-layanan" src="/images/fisio.png" />
                                            <div class="content">
                                                <h3>Fisiotherapy</h3>
                                                <p>Pemulihan fungsi anggota tubuh pasca sakit sangat dibutuhkan, dan kami hadir untuk lebih dekat dengan anda.</p>
                                            </div>
                                        </article>
                                         <article>
                                            <img class="icon-layanan" src="/images/katering.png" />
                                            <div class="content">
                                                <h3>Ahli Gizi dan Catering Diet</h3>
                                                <p>Untuk menunjang kesembuhan kesehatan anda dibutuhkan asupan nutrisi yang baik, kami layani diet sehat anda 
                                                 melalui konsultasi ahli gizi, makanan diet, makanan sehat dan makanan..</p>
                                            </div>
                                        </article>
                                         <article>
                                            <img class="icon-layanan" src="/images/ambulance.png" />
                                            <div class="content">
                                                <h3>Ambulance</h3>
                                                <p>Kebutuhan akan transportasi juga kami hadirkan melalui layanan ambulan orang sakit dan ambulan jenazah.</p>
                                            </div>
                                        </article>
                                    </div>
                                </section>

                        </div>

                        <div id="slider-section">                            
                             <div id="slider-bg"></div>
                           

                             <div class="row full">
                                <div class="col-md-5">
                                     <h2>MENGAPA HARUS<br />
                                     RAWAT SEHAT ?</h2> 
                                </div> 
                                <div class="col-md-2"> 

                                    <img id="hp" src="/images/hp.png"> 
                                
                                    <div class=" carousel1 carousel slide" data-ride="carousel">
                        
                                    <div class="carousel-inner" role="listbox">
                                      <div class="item active">
                                      <img src="/images/gambar1.jpg" alt="Chania">
                                    </div>

                                    <div class="item">
                                        <img src="/images/gambar2.jpg" alt="Chania">
                                    </div>
                                    </div>
                                    </div>
                                   
                                    <!-- -->  

                                </div> 
                                <div class="col-md-3">
                                     <div class="carousel slide" data-ride="carousel">
                        
                                    <div class="carousel-inner" role="listbox">
                                      <div class="item active">
                                      <p>1. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>

                                    <div class="item">
                                        <p>2. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip</p>
                                    </div>
                                    </div>
                                    </div>
                                    <div class="btn-carousel">
                                    <a  href=".carousel " role="button" data-slide="prev">
                                      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a  href=".carousel" role="button" data-slide="next">
                                      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                      <span class="sr-only">Next</span>
                                    </a>
                                    </div>
                                </div>
                            </div>
                            <div  id="quotes-section" class="row">
                                
                            <div id="myCarousel2" class="carousel slide col-md-12" data-ride="carousel"> 
                            <h2>Kata Mereka</h2>

                             <div class="carousel-inner" role="listbox">
                                   <div class="item active row">
                                      <div class="col-md-3">
                                        <img src="/images/dude.svg">
                                      </div>
                                      <div class="col-md-9">
                                        <p class="quotes">Dengan adanya rawat sehat akan membantu masyarakat indonesia</p>
                                      <p><b> Dr Casey Kasianto, Dokter Spesialis paru</b></p>
                                      </div>
                                    </div>

                                    <div class="item row">
                                      <div class="col-md-3">
                                        <img src="/images/dude.svg">
                                      </div>
                                      <div class="col-md-9">
                                        <p class="quotes">Rawat sehat sangat membansu saya dalam merawat ibu saya</p>
                                      <p><b> Muhammad Ali, pasien</b></p>
                                      </div>
                                    </div>

                             </div>

                             <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                             </a>
                             <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                             </a>

                            </div>


                            </div>
                            <div  id="gallery-section">
                                
                                 <div class="row gallery gallery-item"">

                                    <div class="col-md-3 gallery-item judul">                                      
                                      <h2>Gallery</h2>      
                                    </div>
                                     <div class="col-md-3 gallery-item">
                                      <a href="#"><img class="grey" src="images/p1.jpg" alt="" /></a>
                                    </div>
                                     <div class="col-md-3 gallery-item">
                                         <a href="#"><img class="grey" src="images/p2.jpg" alt="" /></a>
                                    </div>
                                     <div class="col-md-3 gallery-item">
                                         <a href="#"><img class="grey" src="images/p3.jpg" alt="" /></a>
                                    </div>
                                  </div>
                                  <div class="row gallery-item"">
                                     <div class="col-md-3 gallery-item">
                                        <a href="#"><img class="grey" src="images/p4.jpg" alt="" /></a>
                                    </div>
                                     <div class="col-md-3 gallery-item">
                                        <a href="#"><img class="grey" src="images/p5.jpg" alt="" /></a>
                                    </div>
                                     <div class="col-md-3 gallery-item">
                                        <a href="#"><img class="grey" src="images/p6.jpg" alt="" /></a>
                                    </div>
                                     <div class="col-md-3 gallery-item">
                                         <a href="#"><img class="grey" src="images/p7.jpg" alt="" /></a>
                                    </div>

                                  </div>
                            </div>
                        </div>
                            
                        <div class="inner">

                            <section>
                                    <div class="posts">
                                        <article>

                                        <header class="major">
                                         <h2>Artikel Kesehatan</h2>
                                         </header>
                                            <ul class="actions">
                                                <li><a href="<?php echo url('/article'); ?>" class="button">Lihat Semua</a></li>
                                            </ul>
                                        </article>
                                        @foreach ($articles as $article)
                                        <article>
                                            <a href="#" class="image"><img src="{{$article->thumbnail_path}}" alt="" /></a>
                                            <h3>{{$article->title}}</h3>
                                            <p>{!! $article->body !!}</p>
                                            <ul class="actions">
                                                <li><a href="{{ url('/article/'.$article->id) }}" class="button">More</a></li>
                                              </ul>
                                        </article>
                                        @endforeach
                                    </div>
                                </section>                         
                        </div>
                      
@stop