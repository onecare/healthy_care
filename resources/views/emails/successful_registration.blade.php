<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css" rel="stylesheet" media="all">
        /* Media Queries */
        @media only screen and (max-width: 500px) {
            .button {
                width: 100% !important;
            }
        }
    </style>
</head>

<?php

$style = [
    /* Layout ------------------------------ */

    'body' => 'margin: 0; padding: 0; width: 100%; background-color: #F2F4F6;',
    'email-wrapper' => 'background: #FFF; color: #333; font: 15px \'Helvetica Neue\', Arial, Helvetica; margin: 0; padding: 0; width: 100%;',

    /* Masthead ----------------------- */

    'email-body' => 'background: #ffffff;',
    // 'email-body_inner' => 'border: none; margin: 50px auto; padding: 0 18px; width: 950px;',
    'email-body_inner' => 'border: none; margin: 50px auto; padding: 0 18px; width: 700px;',

    /* Body ------------------------------ */

    'body_action' => 'background: #fff; border-bottom-left-radius: 4px; border-bottom-right-radius: 4px; padding: 18px;',

    /* Type ------------------------------ */

    'header-1' => 'margin-top: 0; color: #3aa54c; font-size: 20px; font-weight: bold; text-align: left;',
    'header-2' => 'margin-top: 0; font-size: 20px; font-weight: bold; text-align: left;',
    'paragraph' => 'color: #333; font: 15px/1.25em;',
    'paragraph-center' => 'margin-top:50px;color: #333; font: 15px/1.25em; text-align:center',

    /* Buttons ------------------------------ */

    'button' => 'background: #3aa54c; border-radius: 3px; color: #fff; display: block; font-size: 16px; font-weight: 700; line-height: 1.25em; 
    margin: 24px auto 24px; padding: 10px 18px; text-align: center; text-decoration: none; width: 180px;',
];
?>

<body style="{{ $style['body'] }}">
    <table style="{{ $style['email-wrapper'] }}" cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr width="100%">
                <td valign="top" align="left" style="{{ $style['email-body'] }}">
                    <table style="{{ $style['email-body_inner'] }}">
                        <tbody>
                            <tr width="100%">
                                <td valign="top" align="left" style="{{ $style['body_action'] }}">
                                    <h1 style="{{ $style['header-1'] }}"> Registrasi Berhasil </h1>
                                    <p style="{{ $style['paragraph'] }}">
                                        Hai <b>{{ $register->first_name }} {{ $register->last_name }}</b>, <br>
                                        Terima kasih telah melakukan registrasi sebagai tenaga medis di <b>"Rawat Sehat"</b> pada tanggal {{ $date }} pukul {{ $time }}.
                                    </p>

                                    <p style="{{ $style['paragraph'] }}">
                                        Kode Registrasi anda : <h1 style="{{ $style['header-2'] }}"> {{ $register_code }} </h1>
                                    </p>

                                    <p>
                                        Untuk keperluan pendaftaran, kami membutuhkan dokumen administrasi sebagai berikut :
                                        <ol>
                                            <li>Pas foto 4 x 6 = 3 lembar</li>
                                            <li>Copy KTP</li>
                                            <li>Copy NPWP</li>
                                            <li>Copy SIM</li>
                                            <li>Copy Ijazah terlegalisir</li>
                                            <li>Copy STR</li>
                                            <li>Copy SIP</li>
                                            <li>Copy Kartu anggota Profesi (IDI/PPNI/IBI/IFI)</li>
                                            <li>Sertifikasi Keahlian</li>
                                            <li>Sertifikasi pengembangan</li>
                                            <li>Pengalaman kerja/SK Pengangkatan Kerja</li>
                                            <li>Satu buah materai 6000</li>
                                        </ol>
                                    </p>
                                    <p style="{{ $style['paragraph'] }}">
                                        Mohon untuk melengkapi semua dokumen yang sudah di tentukan. Untuk <b>Verifikasi Dokumen</b> anda harus datang langsung ke kantor cabang Rawat Sehat terdekat.
                                    </p>
                                    
                                    <p style="color: #939393; font: 15px/1.25em 'Helvetica Neue', Arial, Helvetica; margin-bottom: 0">
                                        Salam Hangat.
                                    </p>
                                    <img src="http://api.server.rawatsehat.net/assets/img/rawat-sehat/Logo-Rawat-Sehat.png" alt="logo" width="180" height="50">

                                    <p style="{{ $style['paragraph-center'] }}">
                                        Email ini tidak bisa menerima email masuk. <br>
                                        Untuk pertanyaan lebih lanjut hubungi kami di <u>info@rawatsehat.com</u>
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>