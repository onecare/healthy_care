<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LegalInfo extends Model
{
    protected $table = 'legal_info';

    public function translation()
    {
        return $this->hasMany('App\Models\LegalInfoTranslation', 'info_id');
    }
}
