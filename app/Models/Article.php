<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use League\CommonMark\CommonMarkConverter;
use Carbon\Carbon;

class Article extends Model
{
	protected $dates = ['published_at'];

	protected function markdown()
	{
		$converter = new CommonMarkConverter();
		return $converter;
	}

	public function getPublishedAtAttribute()
    {
        $date = date_create($this->attributes['published_at']);
        return $this->attributes['published_at'] = date_format($date, 'd-m-Y');
    }

	public function getPublishedAtLocalizedAttribute()
    {
        setlocale(LC_TIME, 'id_ID.UTF-8');
        return Carbon::parse($this->attributes['published_at'])->formatLocalized('%d %B %Y');
    }

	public function getBodyHtmlAttribute()
	{
		return $this->attributes['body'] = $this->markdown()->convertToHtml($this->attributes['body']);
	}

	public function getExcerptHtmlAttribute()
	{
		
		return $this->attributes['excerpt'] = $this->markdown()->convertToHtml($this->attributes['excerpt']);
	}
}
