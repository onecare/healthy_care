<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    protected $table = 'cities';

    public function province()
    {
        return $this->belongsTo(
            'App\Models\Province'
        );
    }
}
