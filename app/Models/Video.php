<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use League\CommonMark\CommonMarkConverter;
use Carbon\Carbon;

class Video extends Model
{
    protected $dates = ['published_at'];

    protected function markdown()
	{
		$converter = new CommonMarkConverter();
		return $converter;
	}

    public function getDescriptionHtmlAttribute()
	{
		return $this->attributes['description'] = $this->markdown()->convertToHtml($this->attributes['description']);
	}

    public function getPublishedAtAttribute()
    {
        $date = date_create($this->attributes['published_at']);
        return $this->attributes['published_at'] = date_format($date, 'd-m-Y');
    }

    public function getPublishedAtDiffAttribute()
    {
        return $this->attributes['published_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['published_at'])->diffForHumans();
    }

    public function getPublishedAtLocalizedAttribute()
    {
        setlocale(LC_TIME, 'id_ID.UTF-8');
        return Carbon::parse($this->attributes['published_at'])->formatLocalized('%d %B %Y');
    }
}
