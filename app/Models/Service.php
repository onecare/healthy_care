<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use League\CommonMark\CommonMarkConverter;

class Service extends Model
{
    protected function markdown()
	{
		$converter = new CommonMarkConverter();
		return $converter;
	}

	public function getDescriptionHtmlAttribute()
	{
		return $this->attributes['description'] = $this->markdown()->convertToHtml($this->attributes['description']);
	}
}
