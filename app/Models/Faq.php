<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public function translation()
    {
        return $this->hasMany('App\Models\FaqTranslation', 'faq_id');
    }
}
