<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use League\CommonMark\CommonMarkConverter;

class FaqTranslation extends Model
{
    protected $table = 'faqs_translation';

    protected function markdown()
	{
		$converter = new CommonMarkConverter();
		return $converter;
	}

    public function getAnswerHtmlAttribute()
	{
		return $this->attributes['answer'] = $this->markdown()->convertToHtml($this->attributes['answer']);
	}
}
