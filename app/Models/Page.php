<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use League\CommonMark\CommonMarkConverter;
use Baum\Node;

class Page extends Node
{
    protected function markdown()
	{
		$converter = new CommonMarkConverter();
		return $converter;
	}
	
	public function getContentHtmlAttribute()
	{
		return $this->attributes['content'] = $this->markdown()->convertToHtml($this->attributes['content']);
	}
}
