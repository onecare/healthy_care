<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Register extends Model
{
    protected $fillable = [
        'register_code',
        'service_id',
        'identity_number',
        'first_name',
        'last_name',
        'email',
        'gender',
        'date_of_birth',
        'birthplace',
        'address',
        'city',
        'phone',
        'religion',
        'marital_status',
        'last_education',
        'education_place_id',
        'education_place',
        'graduation_year',
        'str_number',
        'expired_date_str',
        'sip_number',
        'expired_date_sip',
        'profession_card_number',
        'workplace',
        'work_experience',
        'status',
    ];

    public function setDateOfBirthAttribute($date)
    {
    	$this->attributes['date_of_birth'] = Carbon::parse($date);
    }
    
    public function setExpiredDateStrAttribute($date)
    {
    	$this->attributes['expired_date_str'] = Carbon::parse($date);
    }

    public function setExpiredDateSipAttribute($date)
    {
    	$this->attributes['expired_date_sip'] = Carbon::parse($date);
    }
}
