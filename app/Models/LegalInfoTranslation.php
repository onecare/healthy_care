<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use League\CommonMark\CommonMarkConverter;

class LegalInfoTranslation extends Model
{
    protected $table = 'legal_info_translation';

    protected function markdown()
	{
		$converter = new CommonMarkConverter();
		return $converter;
	}

    public function legal()
    {
        return $this->belongsTo('App\Models\LegalInfo', 'info_id');
    }
    
	public function getBodyHtmlAttribute()
	{
		return $this->attributes['body'] = $this->markdown()->convertToHtml($this->attributes['body']);
	}
}
