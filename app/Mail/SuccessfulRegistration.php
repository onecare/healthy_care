<?php

namespace App\Mail;

use App\Models\Register;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuccessfulRegistration extends Mailable
{
    use Queueable, SerializesModels;

    public $register;
    public $date;
    public $time;
    public $register_code;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Register $register, $register_code)
    {
        $this->register = $register;
        $this->date = date('d-m-Y');
        $this->time = date('H:i');
        $this->register_code = $register_code;
        $this->subject = 'Registrasi Berhasil';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.successful_registration');
    }
}
