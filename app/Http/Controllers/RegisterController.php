<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SuccessfulRegistration;
use App\Http\Requests\StoreRegisterRequest;
use App\Models\Register;
use App\Models\Service;
use Carbon\Carbon;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRegisterRequest $request, Register $register)
    {        
        $input = $request->all();

        $dateOfBirth = Carbon::create($input['year'], $input['month'], $input['day'])->toDateString();
        $input['date_of_birth'] = $dateOfBirth;
        
        $service_id = $input['service_id'];
        $service = Service::findOrFail($service_id);

        do {

            /* random register code based on services*/
            $register_code = $service->code . str_random('7');

            /* check availability register code*/
            $reg_code = $register->where('register_code', $register_code)->first();

        } while (!empty($reg_code));

        $input['register_code'] = $register_code;

        // temporarily default in use for data collection
        $input['education_place_id'] = 1;

        $email = new SuccessfulRegistration(new Register($request->all()), $register_code);

        Mail::to($request->email)->send($email);

        try {
            $register->create($input);
        } catch (\Exception $exception) {
            return $exception;
            flash()->error('<b>Oops!</b> something wrong.');
            return back();
        }       
        
        flash()->success('<b>Registrasi Berhasil</b>, silahkan cek email anda untuk informasi selengkapnya.');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
