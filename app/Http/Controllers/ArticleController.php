<?php

namespace App\Http\Controllers;
use App\Models\Article;
use Illuminate\Http\Request;


class ArticleController extends Controller
{
    //
     public function index()
    {
    	$articles = Article::orderBy('created_at', 'desc')->paginate(15);
        return view('frontend.articles.index', ['articles' => $articles]);
    }

      public function show($id)
    {
    	$article = Article::find($id);
        return view('frontend.articles.detail', ['article' => $article]);
    }
}
