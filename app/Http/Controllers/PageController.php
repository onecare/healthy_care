<?php

namespace App\Http\Controllers;
use App\Models\Article;
use App\Models\Page;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function show(Page $page, array $parameters)
    {
        $this->prepareTemplate($page, $parameters);
        
        return view('page', compact('page'));
    }

    public function prepareTemplate(Page $page, array $parameters)
    {
        $templates = config('cms.templates');

        if (! $page->template || ! isset($templates[$page->template])) {
            return;
        }
        
        $template = app($templates[$page->template]);
        
        $view = sprintf('templates.%s', $template->getView());

        if (! view()->exists($view)) {
            return;
        }
        
        $template->prepare($view = view($view), $parameters);

        $page->view = $view;
    }
}
