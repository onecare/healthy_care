<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_id' => 'required',
            'identity_number' => 'required|min:16|max:16',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'day' => 'required|min:1|max:31',
            'month' => 'required',
            'year' => 'required|date_format:Y',
            'birthplace' => 'required',
            'religion' => 'required',
            'marital_status' => 'required',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required',
            'email' => 'required|email|max:255|unique:users|unique:registers',
            'last_education' => 'required',
            // 'education_place_id' => 'required',
            'education_place' => 'required',
            'graduation_year' => 'required|date_format:Y',
            'str_number' => 'required',
            'expired_date_str' => 'required|date_format:d-m-Y',
            'sip_number' => 'required',
            'expired_date_sip' => 'required|date_format:d-m-Y',
            'profession_card_number' => 'required',
            'workplace' => 'required',
            'work_experience' => 'required',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'service_id.required' => ':attribute tidak boleh kosong.',
            'identity_number.required' => ' wajib diisi.',
            'identity_number.min' => ' tidak boleh kurang dari :min digit.',
            'identity_number.max' => ' tidak boleh lebih dari :max digit.',
            'first_name.required' => ' wajib diisi.',
            'last_name.required' => ' wajib diisi.',
            'gender.required' => ' wajib diisi.',
            'day.required' => ' wajib diisi.',
            'day.date_format' => ' tidak sesuai format.',
            'month.required' => ' wajib diisi.',
            'year.required' => ' wajib diisi.',
            'year.date_format' => ' tidak sesuai format.',
            'birthplace.required' => ' wajib diisi.',
            'religion.required' => ' wajib diisi.',
            'marital_status.required' => ' wajib diisi.',
            'address.required' => ' wajib diisi.',
            'city.required' => ' wajib diisi.',
            'phone.required' => ' wajib diisi.',
            'email.required' => ' wajib diisi.',
            'email.email' => ' tidak valid.',
            'email.unique' => ' sudah terdaftar. Apakah kamu sudah pernah melakukan registrasi sebelumnya?',
            'last_education.required' => ' wajib diisi.',
            // 'education_place_id.required' => ' wajib diisi.',
            'education_place.required' => ' wajib diisi.',
            'graduation_year.required' => ' wajib diisi.',
            'graduation_year.date_format' => ' tidak sesuai format.',
            'str_number.required' => ' wajib diisi.',
            'expired_date_str.required' => ' wajib diisi.',
            'expired_date_str.date_format' => ' tidak sesuai format.',
            'sip_number.required' => ' wajib diisi.',
            'expired_date_sip.required' => ' wajib diisi.',
            'expired_date_sip.date_format' => ' tidak sesuai format.',
            'profession_card_number.required' => ' wajib diisi.',
            'workplace.required' => ' wajib diisi.',
            'work_experience.required' => ' wajib diisi.',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'service_id' => '<b>Pelayanan</b>',
            'identity_number' => '<b>NIK</b>',
            'first_name' => '<b>Nama Depan</b>',
            'last_name' => '<b>Nama Belakang</b>',
            'gender' => '<b>Gender</b>',
            'day' => '<b>Tanggal Lahir</b>',
            'month' => '<b>Bulan Lahir</b>',
            'year' => '<b>Tahun Lahir</b>',
            'birthplace' => '<b>Tempat Lahir</b>',
            'religion' => '<b>Agama</b>',
            'marital_status' => '<b>Status Pernikahan</b>',
            'address' => '<b>Alamat</b>',
            'city' => '<b>Kota</b>',
            'phone' => '<b>Nomor Telepon</b>',
            'email' => '<b>Email</b>',
            'last_education' => '<b>Pendidikan Terakhir</b>',
            // 'education_place_id' => '<b>Tempat Pendidikan</b>',
            'education_place' => '<b>Tempat Pendidikan</b>',
            'graduation_year' => '<b>Tahun Lulus</b>',
            'str_number' => '<b>No. STR</b>',
            'expired_date_str' => '<b>Masa Berlaku</b>',
            'sip_number' => '<b>No. SIP</b>',
            'expired_date_sip' => '<b>Masa Berlaku</b>',
            'profession_card_number' => '<b>No. Anggota Profesi</b>',
            'workplace' => '<b>Tempat Bekerja</b>',
            'work_experience' => '<b>Pengalaman Kerja</b>',
        ];
    }
}
