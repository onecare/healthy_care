<?php

namespace App\Templates;

use App\Models\LegalInfo;
use App\Models\LegalInfoTranslation;
use Illuminate\View\View;

class LegalPrivacyTemplate extends AbstractTemplate
{
    protected $view = 'legal.privacy';

    protected $legalInfo, $translation;

    public function __construct(LegalInfo $legalInfo, LegalInfoTranslation $translation)
    {
        $this->legalInfo = $legalInfo;
        $this->translation = $translation;
    }

    public function prepare(View $view, array $parameters)
    {
        $legalInfo = $this->legalInfo->where('label', 'privacy')->first();
        
        $translations = $legalInfo->translation()->where('language_code', 'id')->get();
        
        $view->with('translations', $translations);
    }
}