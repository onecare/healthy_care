<?php

namespace App\Templates;

use App\Models\Page;
use Illuminate\View\View;

class AboutTemplate extends AbstractTemplate
{
    protected $view = 'about';

    protected $pages;

    public function __construct(Page $pages)
    {
        $this->pages = $pages;
    }

    public function prepare(View $view, array $parameters)
    {
        $pages = $this->pages->where('template', 'about')->first();

        $view->with('pages', $pages);
    }
}