<?php

namespace App\Templates;

use App\Models\Video;
use Illuminate\View\View;

class VideosTemplate extends AbstractTemplate
{
    protected $view = 'videos';

    protected $videos;

    public function __construct(Video $videos)
    {
        $this->videos = $videos;
    }

    public function prepare(View $view, array $parameters)
    {
        $videos = $this->videos->latest()->paginate(9);
        
        $view->with('videos', $videos);
    }
}