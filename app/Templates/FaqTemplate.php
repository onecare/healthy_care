<?php

namespace App\Templates;

use App\Models\Faq;
use App\Models\FaqTranslation;
use Illuminate\View\View;

class FaqTemplate extends AbstractTemplate
{
    protected $view = 'faq';

    protected $faq, $translation;

    public function __construct(Faq $faq, FaqTranslation $translation)
    {
        $this->faq = $faq;
        $this->translation = $translation;
    }

    public function prepare(View $view, array $parameters)
    {
        //$faq = $this->faq->where('label', 'privacy')->first();
        
        $translations = $this->translation->where('language_code', 'id')->get();
        //dd($translations);
        $view->with('translations', $translations);
    }
}