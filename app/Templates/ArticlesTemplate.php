<?php

namespace App\Templates;

use App\Models\Article;
use Illuminate\View\View;

class ArticlesTemplate extends AbstractTemplate
{
    protected $view = 'articles';

    protected $articles;

    public function __construct(Article $articles)
    {
        $this->articles = $articles;
    }

    public function prepare(View $view, array $parameters)
    {
        $articles = $this->articles->latest()->paginate(9);

        $view->with('articles', $articles);
    }
}