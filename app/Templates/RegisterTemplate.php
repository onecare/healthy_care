<?php

namespace App\Templates;

use Illuminate\View\View;
use App\Models\Service;
use App\Models\City;

class RegisterTemplate extends AbstractTemplate
{
    protected $view = 'register';

    public function prepare(View $view, array $parameters)
    {
        $url= url()->current();

        if (str_contains($url, 'bidan')) {
            $service = 'bidan';
        } elseif (str_contains($url, 'dokter')) {
            $service = 'dokter';
        } elseif (str_contains($url, 'perawat')) {
            $service = 'perawat';
        } elseif (str_contains($url, 'fisioterapi')) {
            $service = 'fisioterapi';
        } elseif (str_contains($url, 'gizi')) {
            $service = 'ahli gizi';
        } elseif (str_contains($url, 'penunggu-pasien')) {
            $service = 'penunggu pasien';
        }
        
        $services = \DB::table('services')->where('name', 'like', $service)->pluck('name', 'id');
        $service_img = Service::where('name', 'like', $service)->first();
        
        //$education_place = \DB::table('educational_places')->pluck('name', 'id');

        /* Displaying the city of Sidoarjo, Surabaya & Gresik */
        $cities = City::with('province')->where('province_id', '35')->whereIn('id',['3515', '3525', '3578'])->pluck('city_name', 'city_name');

        $view->with('services', $services)->with('cities', $cities)->with('service_img', $service_img);
    }
}