<?php

namespace App\Templates;

use App\Models\Article;
use Illuminate\View\View;

class ArticlePostTemplate extends AbstractTemplate
{
    protected $view = 'article.post';

    protected $articles;

    public function __construct(Article $articles)
    {
        $this->articles = $articles;
    }

    public function prepare(View $view, array $parameters)
    {
        $article = $this->articles->where('slug', $parameters['slug'])->first();
        $articles = $this->articles->latest()->paginate(3);

        $view->with('articles', $articles)->with('article', $article);
    }
}