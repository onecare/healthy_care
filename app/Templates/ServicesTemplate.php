<?php

namespace App\Templates;

use App\Models\Service;
use Illuminate\View\View;

class ServicesTemplate extends AbstractTemplate
{
    protected $view = 'services';

    protected $services;

    public function __construct(Service $services)
    {
        $this->services = $services;
    }

    public function prepare(View $view, array $parameters)
    {
        $services = $this->services->orderBy('ordinal_number')->get();
        
        $view->with('services', $services);
    }
}