<?php

namespace App\Templates;

use App\Models\Page;
use App\Models\Testimonial;
use Illuminate\View\View;

class HomeTemplate extends AbstractTemplate
{
    protected $view = 'home';

   protected $pages;

    public function __construct(Page $pages)
    {
        $this->pages = $pages;
    }

    public function prepare(View $view, array $parameters)
    {
        $pages = $this->pages->where('template', 'home')->first();
        $about = $this->pages->where('template', 'about')->first();
        $testimonials = Testimonial::all();
        // dd($testimonials);
        $view->with('pages', $pages)->with('about', $about)->with('testimonials', $testimonials);
    }
}