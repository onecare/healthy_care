<?php

namespace App\Templates;

use App\Models\Video;
use Illuminate\View\View;

class VideoPostTemplate extends AbstractTemplate
{
    protected $view = 'video.post';

    protected $videos;

    public function __construct(Video $videos)
    {
        $this->videos = $videos;
    }

    public function prepare(View $view, array $parameters)
    {
        $video = $this->videos->where('slug', $parameters['slug'])->first();
        $videos = $this->videos->latest()->paginate(3);

        $view->with('videos', $videos)->with('video', $video);
    }
}