<?php

return [
    'templates' => [
        'home' => App\Templates\HomeTemplate::class,
        'about' => App\Templates\AboutTemplate::class,
        'services' => App\Templates\ServicesTemplate::class,
        'articles' => App\Templates\ArticlesTemplate::class,
        'article.post' => App\Templates\ArticlePostTemplate::class,
        'videos' => App\Templates\VideosTemplate::class,
        'video.post' => App\Templates\VideoPostTemplate::class,
        'legal.privacy' => App\Templates\LegalPrivacyTemplate::class,
        'legal.terms' => App\Templates\LegalTermsTemplate::class,
        'contact' => App\Templates\ContactTemplate::class,
        'register' => App\Templates\RegisterTemplate::class,
        'faq' => App\Templates\FaqTemplate::class,
    ]
];