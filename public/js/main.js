$(document).ready(function() {

    $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    
    /* ======= Blog Featured Post Slideshow - Flexslider ======= */ 
    $('.img-slider').flexslider({
        animation: "slide",
        smoothHeight: true
    });

    /* ======= Testimonial Bootstrap Carousel ======= */
    $('#testimonials-carousel').carousel({
      interval: 8000 
    });

    /* ======= Articles & Videos page masonry ======= */
    var $container = $('#article-mansonry, #video-mansonry');
    $container.imagesLoaded(function(){
        $container.masonry({
            itemSelector : '.post'
        });
    });

    /* ======= Form Register ======= */
    $('#city').select2({
        placeholder: "Kota",
        allowClear: true
    });

    $('#register-btn').on('click', function() {
        var $btn = $(this).button('loading');
    });

    $('input[name=expired_date_str]').datetimepicker({
        allowInputToggle: true,
        format: 'DD-MM-YYYY',
        showClear: true,
        showClose: true,
        minDate:new Date(),
        widgetPositioning: {
            vertical: 'bottom'
        }
    });

    $('input[name=expired_date_sip]').datetimepicker({
        allowInputToggle: true,
        format: 'DD-MM-YYYY',
        showClear: true,
        showClose: true,
        minDate:new Date(),
        widgetPositioning: {
            vertical: 'bottom'
        }
    });
});