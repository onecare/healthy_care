jQuery(window).load( function($) {

//
  // Flexslider
  //
  if( jQuery(".flexslider").length > 0) {
    jQuery(".flexslider").flexslider({
      'controlNav': true,
      'directionNav': true
    });
  }

  jQuery('#entry-listing').isotope({
    animationOptions: {
      duration: 750,
      easing: 'linear',
      queue: false
    },
    itemSelector: 'article.entry',
    transformsEnabled: false
  });

});

    
$(document).ready(function($) {
			
  		$(".forgot_password").click(function() {
        var options = {
        	buttons: {
        		confirm: {
        			text: 'Kirim',
        			className: 'blue',
        			action: function(e) {
        				Apprise('close');
                $.get( settings.base_url+ "home/forgot_password", { email: e.input } )
                  .done(function( data ) {
                    var n = noty({
						            text        : 'Silahkan check email anda',
						            type        : 'success',
						            dismissQueue: true,
						            layout      : 'topRight',
						            theme       : 'relax'
						        });
                  });
        			}
        		},
        	},
        	input: true,
        };
	   Apprise('Email Anda:', options);
    });

      //$('#curhatimage a').mouseover(function(event) {
        //$(this).css('color', '#fff !important');
        /*$('#helpwoman').show().slideDown(10000);*/
      //});
      
      $( ".search_button" ).click(function() {
          $('#searchform2').show();
          $(this).hide();
      });

      $("#curhat").bind("click", function(e){
          $("#curhat").animate({height: 180}, 'slow');
          $(".curhat_button").show();
      });

      $(".curhat_cancel").click(function(){
        $("#curhat").animate({height: 50}, 'slow');
        $(".curhat_button").hide();
      });

    $(document).click(function(e) {
        $('#helpwoman').hide();
        if ($('#login-content').is(':visible'))
        {
          var target = e.target.tagName;
          if (target != 'INPUT')
            $('#login-content').hide();
        }
    });

    var playlist = [
        {
            "title": "Kaskus",
            "mp3": "http://ibunda.onlivestreaming.net:9889/stream"
        }
    ];

    new jPlayerPlaylist({
        jPlayer: "#jquery_jplayer_1",
        cssSelectorAncestor: "#jp_container_1"
    }, playlist, {
        swfPath: "jPlayer",
        supplied: "mp3",
        wmode: "window",
        preload: "none",
        keyEnabled: true,
        solution: 'html, flash',
        volume: 0.8,
        muted: false,
        backgroundColor: '#000000',
        cssSelectorAncestor: '#jp_container_1',
        cssSelector: {
        videoPlay: '.jp-video-play',
        play: '.jp-play',
        pause: '.jp-pause',
        stop: '.jp-stop',
        seekBar: '.jp-seek-bar',
        playBar: '.jp-play-bar',
        mute: '.jp-mute',
        unmute: '.jp-unmute',
        volumeBar: '.jp-volume-bar',
        volumeBarValue: '.jp-volume-bar-value',
        volumeMax: '.jp-volume-max',
        playbackRateBar: '.jp-playback-rate-bar',
        playbackRateBarValue: '.jp-playback-rate-bar-value',
        currentTime: '.jp-current-time',
        duration: '.jp-duration',
        fullScreen: '.jp-full-screen',
        restoreScreen: '.jp-restore-screen',
        repeat: '.jp-repeat',
        repeatOff: '.jp-repeat-off',
        gui: '.jp-gui',
        noSolution: '.jp-no-solution'
        },
        errorAlerts: false,
        warningAlerts: false
    });
  var $menu = $("#navigation");
  $menu.superfish({
    animation: {
      opacity: "show",
      height: "show"
    },
    speed: "fast",
    delay: 250
  });


      $('#new-tags-text').keypress(function(e){
        if(e.keyCode == 13) {
          var obj = jQuery(this);
          
          var li = jQuery('.selected-tags-example').clone().hide();
          li.removeClass('selected-tags-example').addClass('selected-tags');
          li.find('.selected-tags-title').text(obj.val());
          li.find('.selected-tags-name').val(obj.val());
          li.appendTo('#selected-tags').fadeIn();
          delete li;
          
          obj.val('');
          delete obj;
          
          e.preventDefault();
        }
      });
      
      jQuery(document).on('click', '.selected-tags-remove',function(e){
        jQuery(this).parent().parent().fadeOut().remove();
        //console.log(this);
        e.preventDefault();
      });
      
      
  $(".searchsubmit").bind("click", function() {
    $(this).parent().submit();
  });

  /*$(".fancybox").fancybox({
    fitToView: true
  });

  $("a[data-lightbox^=fancybox]").fancybox({
    fitToView: true
  });*/


  // Responsive menu
  $("<select />").appendTo("nav");

  // Create default option "Go to..."
  $("<option />", {
    "selected": "selected",
    "value": "",
    "text": "Go to..."
  }).appendTo("nav select");

  // Populate dropdown with menu items
  $("nav a").each(function () {
    var el = $(this);
    $("<option />", {
      "value": el.attr("href"),
      "text": el.text()
    }).appendTo("nav select");
  });

  $("nav select").change(function () {
    window.location = $(this).find("option:selected").val();
  });

  $(function(){
    $.fn.formLabels();
  });

  $("#jp500").jPlayer({
    ready: function (event) {
      $(this).jPlayer("setMedia", {
        m4a:"../../../www.jplayer.org/audio/m4a/TSP-01-Cro_magnon_man.m4a",
        oga:"../../../www.jplayer.org/audio/ogg/TSP-01-Cro_magnon_man.ogg"
      });
    },
    swfPath: "js",
    supplied: "m4a, oga",
    wmode: "window",
    cssSelectorAncestor: "#jp-203"
  });

  //fitVids
  $(".inner-container .format-video .entry-image").fitVids();


  $('#styler a').not("#style-toggle a").click(function(){
      var css = $(this).attr('href');
      var logo = $(this).attr('id'); 
      $('#color').attr('href', css);
      $('.logo img').attr('src','demo/' + logo + '.png');
      return false;
  });

  var st = false;
  $('#style-toggle a').click(function(e){
     if (!st) {
         $('#styler').animate({
             left:'+=200'
         },'fast');
         st = true;
     }
      else {
         $('#styler').animate({
             left:'-=200'
         },'fast');
         st = false;
     }
    e.preventDefault();
  });

});

jQuery(window).load( function() {

  // Page width calculations

  jQuery(window).resize(setContainerWidth);
  var $box = jQuery(".box");

  function setContainerWidth() {
    var columnNumber = parseInt((jQuery(window).width()+15) / ($box.outerWidth(true))),
      containerWidth = (columnNumber * $box.outerWidth(true)) - 15;

    if ( columnNumber > 1 )  {
      //jQuery("#box-container").css("width",containerWidth+'px');
    } else {
      jQuery("#box-container").css("width", "100%");
    }

  }

  setContainerWidth();
  loadAudioPlayer();

});

function loadAudioPlayer() {
  jQuery(".format-audio").each(function() {
    var $audio_id = jQuery(this).find(".audio-wrap").data("audio-id"),
      $media = jQuery(this).find(".audio-wrap").data("audio-file"),
      $play_id = '#jp-'+$audio_id,
      $play_ancestor = '#jp-play-'+$audio_id,
      $extension = $media.split('.').pop();

    if ( $extension.toLowerCase() =='mp3' ) {
      var $extension = 'mp3';
    } else if ( $extension.toLowerCase() =='mp4' ||  $extension.toLowerCase() =='m4a' ) {
      var $extension = 'm4a';
    } else if ( $extension.toLowerCase() =='ogg' || $extension.toLowerCase() =='oga' ) {
      var $extension = 'oga';
    } else {
      var $extension = '';
    }


    jQuery($play_id).jPlayer({
      ready: function (event) {
        var playerOptions = {
          $extension: $media
        };
        playerOptions[$extension] = $media;
        jQuery(this).jPlayer("setMedia", playerOptions);
      },
//      swfPath: '../../js', uncomment this for swf support
      supplied: $extension,
      wmode: 'window',
      cssSelectorAncestor: $play_ancestor
    });

  });

/*jQuery("#curhat").click(function () {
  window.location = "http://ibunda.id/home/curhatyuk";
});*/
}