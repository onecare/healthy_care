const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
    mix.copy('node_modules/bootstrap-sass/assets/fonts', 'public/fonts');
    mix.copy('node_modules/font-awesome/fonts', 'public/fonts');
    mix.copy('node_modules/flexslider/fonts', 'public/fonts');

    mix.copy('node_modules/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'public/js/bootstrap.js');
    mix.copy('node_modules/jquery/dist/jquery.min.js', 'public/js/jquery.js');
    mix.copy('node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', 'public/js/bootstrap-datetimepicker.js');
    mix.copy('node_modules/moment/min/moment.min.js', 'public/js/moment.js');
    mix.copy('node_modules/flexslider/jquery.flexslider-min.js', 'public/js/flexslider.js');

    mix.scripts([
        'jquery.js', 'bootstrap.js', 'moment.js', 'bootstrap-datetimepicker.js', 'flexslider.js', 'main.js'
    ], 'public/js/app.js', 'public/js');

    mix.sass('app.scss');
});
